'use strict';

/**
 * @ngdoc function
 * @name demoApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the demoApp
 */
angular.module('demoApp')
  .controller('MainCtrl', function ($scope, $http) {

    $scope.awesomeThings = [
      'this is first awesomeThing',
      'great things',
      'another great thing'
    ];


    $scope.getData = function () {
    	$http({method: 'GET', url: 'getData'}).

  			success(function(data, status, headers, config) {
    			$scope.data = data;
  		  }).

  			error(function(data, status, headers, config) {
    			console.log("Error communicating with the server");
  		  });
    }


  });
