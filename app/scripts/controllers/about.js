'use strict';

/**
 * @ngdoc function
 * @name demoApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the demoApp
 */
angular.module('demoApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.welcomeMessage = 'this is the about text';
  });
