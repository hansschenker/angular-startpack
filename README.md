# Angular Startpack #

This is a simple project skeleton for web app development. 
It is client-side application that uses:

* Javascript 
* NPM
* Bower
* Grunt
* Karma and Jasmine


The project demonstrates a typical layout for the client-side project, uses Grunt to automate most of the tasks, and showcases the use of testing framework, including the mocking of the http backend and how to work with Angular projects.


